module.exports = {
  presets: [
    ["@babel/preset-env",
    {
      useBuiltIns: "entry",
      debug: true,
      corejs: 3,
      modules: false,
      targets: {
        chrome: "58",
        ie: "11"
      }
    }]
  ]
};