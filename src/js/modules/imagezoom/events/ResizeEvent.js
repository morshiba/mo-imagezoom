


class ResizeEvent {

    constructor(callback) {
        this.callback = callback ? callback : null;
        this.updateTImeout = null;
        this.BindEvents();
    }

    BindEvents() {
        window.addEventListener("resize", this.OnResize.bind(this));
    }


    OnResize() {
        if (this.updateTImeout !== null) {
            clearTimeout(this.updateTImeout);
            this.updateTImeout = null;
        }
        this.updateTImeout = setTimeout(function () {
            this.updateTImeout = null;
            this.callback("resize");
        }.bind(this), 800);
    }

}
module.exports = ResizeEvent;