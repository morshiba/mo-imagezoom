



class MouseEventsMobile {

    constructor(target, callback) {
        console.log("mouse over callback", callback);
        this.callback = callback ? callback : null;
        this.target = target;
        this.data = {
            x: 0,
            y: 0
        }
        this.BindEvents();
    }

    BindEvents() {
        let scope = this;
        this.target.addEventListener("click", function() {
            scope.OnTouchStart();
        });
    }


    OnTouchStart() {
        if ( this.callback !== null ) {
            this.callback("over");
        }
    }

    StopAll() {
        console.log("MouseEventsMobile:: StoppAll: ");
        let scope = this;
        this.target.removeEventListener("click", function() {
            scope.OnTouchStart();
        });
    }
}

module.exports = MouseEventsMobile;