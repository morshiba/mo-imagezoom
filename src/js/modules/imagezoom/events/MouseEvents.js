



class MouseEvents {

    constructor(target, callback) {
        this.callback = callback ? callback : null;
        this.target = target;
        this.data = {
            x: 0,
            y: 0
        }
        this.BindEvents();
    }

    BindEvents() {
        this.target.addEventListener("mouseover", this.onMouseOver.bind(this));
        this.target.addEventListener("mouseout", this.onMouseOut.bind(this));
        this.target.addEventListener("mousemove", this.onMouseMove.bind(this));
    }

    StopAll() {
        console.log("MouseEvents:: StoppAll: ");
        this.target.removeEventListener("mouseover", this.onMouseOver.bind(this));
        this.target.removeEventListener("mouseout", this.onMouseOut.bind(this));
        this.target.removeEventListener("mousemove", this.onMouseMove.bind(this));
    }


    onMouseOver(event) {
        this.updateMouseInfo(event);
        // console.log("mouse over this.callback", this.callback);
        if ( this.callback !== null ) {
            this.callback("over", this.data);
        }
    }

    onMouseOut(event) {
        this.updateMouseInfo(event);
        // console.log("mouse out");
        if ( this.callback !== null ) {
            this.callback("out", this.data);
        }
    }

    onMouseMove(event) {
        this.updateMouseInfo(event);

        if ( this.callback !== null ) {
            this.callback("move", this.data);
        }
    }

    updateMouseInfo(event) {
        var eventDoc, doc, body;
        event = event || window.event;
        if (event.pageX == null && event.clientX != null) {
            eventDoc = (event.target && event.target.ownerDocument) || document;
            doc = eventDoc.documentElement;
            body = eventDoc.body;

            event.pageX = event.clientX +
              (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
              (doc && doc.clientLeft || body && body.clientLeft || 0);
            event.pageY = event.clientY +
              (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
              (doc && doc.clientTop  || body && body.clientTop  || 0 );
        }
        this.data.x = event.pageX;
        this.data.y = event.pageY;
    }
}

module.exports = MouseEvents;