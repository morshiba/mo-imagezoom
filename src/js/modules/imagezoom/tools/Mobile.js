

const ORIENTATION_STATES = {
    0: "portrait",
    90: "landscape"
};

class Mobile {

    constructor() {
        this.events = {};
        this.isMobile = /Mobi|Android/i.test(navigator.userAgent);
        this.orientation = "portrait";
        let scope = this;

        let onOrientationChange = function() {
            // console.log("onOrientationChange");
            if (window.matchMedia("(orientation: portrait)").matches) {
                scope.orientation = ORIENTATION_STATES[90];
            }
             
            if (window.matchMedia("(orientation: landscape)").matches) {
                scope.orientation = ORIENTATION_STATES[0];
            }
            if ( scope.events["orientation"] ) {
                // console.log("Mobile:: onOrientationChange: scope.orientation ", scope.orientation);
                scope.events["orientation"](scope.orientation);
            }
            // console.log("onOrientationChange portrait", window.matchMedia("(orientation: portrait)"));
            // console.log("onOrientationChange landscape", window.matchMedia("(orientation: landscape)"));
        }
        window.addEventListener("orientationchange", onOrientationChange, false);
        if (window.matchMedia("(orientation: portrait)").matches) {
            this.orientation = ORIENTATION_STATES[0];
        }
         
        if (window.matchMedia("(orientation: landscape)").matches) {
            this.orientation = ORIENTATION_STATES[90];
        }
    }

    get IsMobile() {
        if ( !this.isMobile ) {
            if ( window.innerWidth < 768 ) {
                return true;
            }
        }
        return this.isMobile;
    }

    get Orientation() {
        return this.orientation;
    }

    AddEvent(type, callback) {
        this.events[type] = callback;
    }
}

module.exports = Mobile;