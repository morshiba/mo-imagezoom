
import ObjectCords from './ObjectCords';

class Image {

    constructor(domElement) {
        this.domElement = domElement;
        this.data = {
            width: 0,
            height: 0,
            top: 0,
            left: 0
        };

        this.Init();
    }

    Init() {
        this.data = new ObjectCords(this.domElement);
    }

    get Width() {
        return this.data.width;
    }
    get Height() {
        return this.data.height;
    }
    get Top() {
        return this.data.top;
    }
    get Left() {
        return this.data.left;
    }
    get DOM() {
        return this.domElement;
    }
    get Src() {
        if (this.domElement.dataset.src ) {
            return this.domElement.dataset.src;
        }
        return this.domElement.src;
    }
}

module.exports = Image;