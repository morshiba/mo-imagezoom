/**
 * @class   ObjectCords
 * @description get top and left position in document and width and height of DOM element
 * @author  Morshiba
 * @license MIT
 * @param   {DomObject} elem  the element you want to watch for
 */

class ObjectCords {

    constructor(elem) {
        var box = this.getBoundingClientRect(elem);

        var body = document.body;
        var docEl = document.documentElement;

        var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
        var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

        var clientTop = docEl.clientTop || body.clientTop || 0;
        var clientLeft = docEl.clientLeft || body.clientLeft || 0;

        var top = box.top + scrollTop - clientTop;
        var left = box.left + scrollLeft - clientLeft;

        return {
            top: Math.round(top),
            Top: Math.round(top),
            left: Math.round(left),
            Left: Math.round(left),
            width: box.width,
            Width: box.width,
            height: box.height,
            Height: box.height
        };
    }

    getBoundingClientRect(element) {
        try {
            return element.getBoundingClientRect();
        } catch (e) {
            if (typeof e === 'object' && e !== null /* && check the e is the exception we expect */) {
                console.log("error e", e);
                return { top: 0, bottom: 0, left: 0, width: 0, height: 0, right: 0 };
            } else {
                throw e; // something else went wrong, and we must surface the error
            }
        }
    }
}
module.exports = ObjectCords;