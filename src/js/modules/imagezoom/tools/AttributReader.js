/**
 * @class   AttributReader
 * @description update the options object with the data found in target DOM elements dataset
 * @author  Morshiba
 * @license MIT
 */

const ObjectCords = require("./ObjectCords");


class AttributReader {

    constructor() {

    }

    /**
     * @function    Parse
     * @description for any element in options object chack if there is a dataset value to update
     * @param {DOMObject} counter the counter html dom object
     * @param {Options} options the default options Object
     */
    Parse(element, options) {
        for (let i in options.AsObject) {
            options[i] = this.GetData(i, options, element);
        }
        return options;
    }

    /**
     * @function    GetData
     * @description test value and return new or default value
     * @param {string} name the attributes name
     * @param {Options} options the default options object
     * @param {DOMObject} counter the counter html object 
     */
    GetData(name, options, element) {
        let setting;
        if (element.dataset[name]) {
            switch (name) {
                case "zoomparent":
                    setting = this.getNumber(element.dataset[name]);
                    break;
                case "zoomposition":
                    if ( element.dataset[name] !== "" ) {
                        setting = element.dataset[name];
                    } 
                    break;
                default:
                    setting = element.dataset[name];
                    break;
            }
        } else {
            setting = options[name];
        }
        if (setting === "") {
            setting = options[name];
        }
        return setting;
    }

    getNumber(number) {
        if (String(number).indexOf(",") > -1) {
            return Number(String(number).split(",").join("."));
        } else {
            if ( String(number) === "" ) {
                return 0;
            }
            return Number(number);
        }
    }
}

module.exports = AttributReader; 