

import ObjectCords from './ObjectCords';


class Options {

    constructor() {
        this.zoomposition = "";
        this.zoomparent = 2;
        this.debug = 0;
        this.image = null;
    }

    
    /**
     * @getter AsObject
     * @returns {object} options all options as object
     */
    get AsObject() {
        return {
            zoomposition: this.ZoomPosition,
            zoomparent: this.ZoomParent,
            debug: this.Debug
        };
    }

    Update(options) {
        let i;
        if ( options ) {
            for ( i in options ) {
                if ( this[i] ) {
                    this[i] = options[i];
                }
            }   
        }
    }

    get ZoomPosition() {
        if ( this.zoomposition !== "" ) {
            return this.zoomposition;
        } else {
            let imageParent = this.image.DOM;  
            let i = 0;
            while ( i < this.ZoomParent ) {
                imageParent = imageParent.parentElement;
                i++;
            }
            let outerView = new ObjectCords(imageParent);
            if ( this.image.Left > outerView.width / 2 ) {
                this.zoomposition = "left";
            } else {
                this.zoomposition = "right";
            }
        }
        return this.zoomposition;
    }

    set ZoomPosition(zoomposition) {
        this.zoomposition = zoomposition;
    }

    get ZoomParent() {
        return this.zoomparent;
    }
    set ZoomParent(zoomparent) {
        this.zoomparent = zoomparent;
    }

    get Debug() {
        return Number(this.debug);
    }
    set Debug(debug) {
        this.debug = debug;
    }

    get Image() {
        return Number(this.image);
    }
    set Image(image) {
        this.image = image;
    }
}

module.exports = Options;