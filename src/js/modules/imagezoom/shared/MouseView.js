


class MouseView {

    constructor() {
        this.image = null;
        this.options = null;
        this.view = null;
        this.cross = null;
    }

    Init(image, options) {
        this.image = image;
        this.options = options;
        this.Build();
    }

    Close() {
        let views = this.view.parentNode.querySelectorAll('.moimagezoomcross');
        if (views.length > 0) {
            for (let i = 0; i < views.length; i++) {
                views[i].parentNode.removeChild(views[i]);
            }
        }
    }

    Build() {
        // console.log("MoseView:: Build:");
        this.view = document.createElement('div');
        this.view.className = "moimagezoomcross";
        this.cross = document.createElement('div');
        this.view.appendChild(this.cross);
        this.image.DOM.parentNode.appendChild(this.view);
    }

    Resize(scaleX, scaleY) {
        this.cross.style.width = Math.abs(Math.round(this.image.Width * scaleX)) + 'px';
        this.cross.style.height = Math.abs(Math.round(this.image.Height * scaleY)) + 'px';
        this.cross.style.top = (parseInt(this.cross.style.height) / 2 * -1) + 'px';
        this.cross.style.left = (parseInt(this.cross.style.width) / 2 * -1) + 'px';
    }

    Move(position) {
        // console.log("offset: this.image", this.image);
        // console.log("offset: position", position);
        if (this.image !== null) {
            let diffLeft = position.x - this.image.Left;
            let diffTop = position.y - this.image.Top;

            
            let percentTop = 1 / this.image.Height * diffTop;
            let percentLeft = 1 / this.image.Width * diffLeft;
            // console.log("Move: this.image.Left, position.x", this.image.Left, position.x);
            // console.log("Move: this.image.Width, diffLeft", this.image.Width, diffLeft);
            // console.log("Move: percentTop, percentLeft", percentTop, percentLeft);
            // console.log("Move: percentTop", percentTop);
            this.cross.style.top = (parseInt(this.cross.style.height) * percentTop * -1) + 'px';
            this.cross.style.left = (parseInt(this.cross.style.width) * percentLeft * -1) + 'px';


            let style = this.image.DOM.parentNode.currentStyle || window.getComputedStyle(this.image.DOM.parentNode);
            // console.log("offset: style", style);
            diffLeft += (style["margin-left"] != "auto" && style["margin-left"] !== "") ? parseInt(style["margin-left"]) : 0;
            diffLeft += (style["padding-left"] != "auto" && style["padding-left"] !== "") ? parseInt(style["padding-left"]) : 0;
            // diffLeft += parseInt(style["padding-left"]);
            // console.log("offset: position, diffLeft, diffTop", position, diffLeft, diffTop);
            this.view.style.left = diffLeft + 'px';
            this.view.style.top = diffTop + 'px';

        }
    }
}

module.exports = MouseView;