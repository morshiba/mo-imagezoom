
import ObjectCords from "../tools/ObjectCords";



class ZoomBase {  

    constructor(image, options) {
        this.image = image;
        this.options = options;
        this.size = {width: 500, height: 500};
        this.view = null;
        this.imageView = null;
        this.imageData = null;
        this.initialImageData = null;
        this.viewParent = document.getElementsByTagName("body")[0];
        this.transition = "all 1s ease";
        // this.viewParent = this.image.DOM.parentNode;
        this.Build();
    }

    Build() {
        this.BuildView("moimagezoomview");
        let loader = this.BuildLoader();
        this.view.appendChild(loader);
        this.imageView = this.BuildImage();
        this.imageData = new ObjectCords(this.imageView);
        this.viewParent.appendChild(this.view);
        this.AddImageLoader(loader)
    }

    Init() {
        // console.log("INIT ZOOOOOOOOM ZoomBase");
        this.initialImageData = new ObjectCords(this.imageView);
        this.imageData = new ObjectCords(this.imageView);
        console.log("ZoomBase:: Init: this.initialImageData.width, this.imageData.width", this.initialImageData.width, this.imageData.width);

        if ( this.initialImageData.width == this.imageData.width ) {
            this.initialImageData.width = this.initialImageData.width * 2;
            this.initialImageData.height = this.initialImageData.height * 2;
            this.imageView.style.width = this.initialImageData.width + 'px';
            this.imageView.style.height = this.initialImageData.height + 'px';
            this.imageData = new ObjectCords(this.imageView);
        }
        this.imageView.style.opacity = 1;
    }

    Close(e) {
        this.viewParent.removeChild(this.view);
    }

    BuildView(className) {
        this.view = document.createElement('div');
        this.view.className = className ? className : "moimagezoomview";
    }

    BuildLoader() {
        let loader = document.createElement('div');
        loader.className = "loader";
        return loader;
    }

    BuildImage() {
        let imageView = document.createElement('img');
        imageView.src = this.image.Src;
        imageView.style.opacity = 0;
        return imageView;
    }

    AddImageLoader(loader) {
        let scope = this;
        this.imageView.onload = function () {
            // console.log("AddImageLoader XXXXXXXXX ZoomBase");
            loader.parentNode.appendChild(scope.imageView);
            loader.parentNode.removeChild(loader);
            // console.log("AddImageLoader XXXXXXXXX ZoomBase");
            scope.Init();
            // console.log("AddImageLoader XXXXXXXXX ZoomBase");
        }
    }
}

module.exports = ZoomBase;