
import ObjectCords from "../tools/ObjectCords";

class ZoomViewPosition {

    constructor() {
        this.view = null;
        this.image = null;
        this.options = null;
        this.width = 0;
        this.height = 0;
        this.top = 0;
        this.left = 0;
        this.initLeft = 0;
    }

    Init(view, image, options) {
        this.view = view;
        this.image = image;
        this.options = options;
        this.GetViewSize();
        this.SetViewPosition(true);
    }

    Resize() {
        this.GetViewSize();
        this.Update();
    }

    Update() {
        // this.GetViewSize();
        this.view.style.left = this.initLeft + "px";
        this.view.style.height = this.height + "px";
        this.view.style.transition = "all 0.4s ease";
        setTimeout(function() {
            this.SetViewPosition();
        }.bind(this), 10);
        // this.SetViewPosition();
    }

    Close() {
        this.view.style.left = this.initLeft + "px";
        this.view.style.width = "0px";
        setTimeout(function() {
            this.view.parentNode.removeChild(this.view);
        }.bind(this), 400);
    }

    GetViewSize() {
        let i = 0;
        let imageParent = this.image.DOM;  
        while ( i < this.options.ZoomParent ) {
            imageParent = imageParent.parentElement;
            i++;
        }
        let outerView = new ObjectCords(imageParent);

        this.top = this.image.Top
        this.height = this.image.Height;
        // this.width = outerView.width - this.image.Left + outerView.left;
        if ( this.options.ZoomPosition == "left" ) {
            // this.width = outerView.Width - this.image.Left;
            this.width = this.image.Left - outerView.left;
            this.left = this.image.Left - this.width;
            this.initLeft = this.left + this.width;
        } else if ( this.options.ZoomPosition == "right" ) {
            this.width = outerView.Width - this.image.Width;
            this.left = this.image.Width + this.image.Left;
            this.initLeft = this.left;
        }
        // console.log("ZoomViewPosition:: GetViewSize: imageParent", imageParent);
        // console.log("ZoomViewPosition:: GetViewSize: outerView", outerView);
        // console.log("ZoomViewPosition:: GetViewSize: this.image.Left, this.image.Width", this.image.Left, this.image.Width);
        // console.log("ZoomViewPosition:: GetViewSize: this.options.ZoomPosition", this.options.ZoomPosition);
        // console.log("ZoomViewPosition:: GetViewSize: this.width, this.left", this.width, this.left);
    }

    SetViewPosition(init) {
        this.view.style.top = this.top + "px";
        this.view.style.left = init ? this.initLeft + 'px' : this.left + "px";
        this.view.style.height = this.height + "px";
        this.view.style.width = this.width + "px";
    }

    get Width() {
        return this.width;
    }

    get Height() {
        return this.height;
    }
}

module.exports = ZoomViewPosition;