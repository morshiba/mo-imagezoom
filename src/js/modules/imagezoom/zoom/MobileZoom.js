

import ZoomBase from "../shared/ZoomBase";
import Mobile from "../tools/Mobile";
import ObjectCords from "../tools/ObjectCords";

class MobileZoom extends ZoomBase {

    constructor(image, options) {
        super(image, options);
        this.size = { height: window.innerHeight, width: window.innerWidth };
        this.position = {
            width: "0px",
            height: "0px",
            top: "0px",
            left: "0px",
            transform: "rotate(0deg)",
            opacity: "1"

        };
        this.mobile = new Mobile();
        this.mobile.AddEvent("orientation", this.OnOrientationChange.bind(this));
    }

    Build() {
        this.BuildView("moimagezoomview mobile");
        this.viewParent.appendChild(this.view);

        this.imageCenterWrapper = document.createElement('div');
        this.imageCenterWrapper.className = "imagewrapper";

        this.view.appendChild(this.imageCenterWrapper);

        let loader = this.BuildLoader();
        this.imageCenterWrapper.appendChild(loader);

        this.closeBtn = this.BuildCloseButton();
        this.closeBtn.addEventListener("click", this.Close.bind(this));

        this.imageView = this.BuildImage();
        this.imageView.style.opacity = 0;
        console.log("this.transition", this.transition);
        // this.imageView.style.transition = this.transition;
        this.AddImageLoader(loader);

        this.imageData = new ObjectCords(this.imageView);
        // console.log("window.scrollTop", window.pageYOffset);
        // this.viewParent.style.overflow = "hidden";
        // document.getElementsByTagName("body")[0].style.overflow = "hidden";

    }

    Close(e) {
        
        let view = this.viewParent.querySelector('.moimagezoomview');
        if ( view ) {
            this.viewParent.removeChild(view);
        }
        // this.viewParent.style.overflow = "initial";
        // document.getElementsByTagName("body")[0].style.overflow = "initial";
        // console.log("window.scrollTop", window.pageYOffset);
    }

    OnOrientationChange(orientation) {
        // console.log("OnOrientationChange:: orientation", orientation);
        setTimeout(function () {
            // this.Scale();
            this.size = { height: window.innerHeight, width: window.innerWidth };
            if (this.size.width < this.size.height) {
                // console.log("OnOrientationChange:: portrait");
                if (this.image.Width > this.image.Height) {
                    // console.log("Queerformat");
                    this.position.transform = 'rotate(90deg)';
                } else {
                    // console.log("Hochformat");
                    this.position.transform = 'rotate(0deg)';
                }
            } else {
                // console.log("OnOrientationChange:: landscape");
                if (this.image.Width > this.image.Height) {
                    // console.log("Queerformat");
                    this.position.transform = 'rotate(0deg)';
                } else {
                    // console.log("Hochformat");
                    this.position.transform = 'rotate(90deg)';
                }
            }
            this.imageCenterWrapper.style.transform = this.position.transform;
            // this.Scale();
            // this.UpdatePosition();
        }.bind(this), 100);
    }

    Scale() {
        let percent;
        if (this.size.width < this.size.height) {
            percent = this.ScalePortrait();
        } else {
            percent = this.ScaleLandscape();
        }
        this.position.height = Math.abs(Math.round(this.initialImageData.Height * percent)) + 'px';
        this.position.width = Math.abs(Math.round(this.initialImageData.Width * percent)) + 'px';
    }

    ScalePortrait() {
        let percent, width, height;
        
        if (this.image.Width > this.image.Height) {
            this.position.transform = 'rotate(90deg)';
            percent = 1 / this.initialImageData.Width * this.size.height;
            height = Math.abs(Math.round(this.initialImageData.Height * percent));
            if ( height > this.size.width ) {
                percent = 1 / this.initialImageData.Height * this.size.width;
            }
        } else {
            percent = 1 / this.initialImageData.Height * this.size.height;
            width = Math.abs(Math.round(this.initialImageData.Width * percent));
            if ( width > this.size.width ) {
                percent = 1 / this.initialImageData.Width * this.size.width;
            }
        }
        return percent;
    }

    ScaleLandscape() {
        let percent, width, height;
        
        if (this.image.Width > this.image.Height) {
            this.position.transform = 'rotate(0deg)';
            percent = 1 / this.initialImageData.Width * this.size.width;
            height = Math.abs(Math.round(this.initialImageData.Height * percent));
            if ( height > this.size.height ) {
                percent = 1 / this.initialImageData.Height * this.size.height;
            }
        } else {
            this.position.transform = 'rotate(90deg)';
            percent = 1 / this.initialImageData.Height * this.size.width;
            width = Math.abs(Math.round(this.initialImageData.Width * percent));
            if ( width > this.size.height ) {
                percent = 1 / this.initialImageData.Width * this.size.height;
            }
        }
        return percent;
    }

    Center() {
        let top, left;
        
        left = (parseInt(this.position.width) / 2);
        top = (parseInt(this.position.height) / 2);
        this.position.top = (-1 * top) + 'px';
        this.position.left = (-1 * left) + 'px';
    }

    UpdatePosition() {
        let style = "";
        this.imageView.setAttribute('style', style);
        this.imageView.style.width = this.position.width;
        this.imageView.style.height = this.position.height;
        this.imageData = new ObjectCords(this.imageView);
        this.imageCenterWrapper.style.transform = this.position["transform"];

        this.Center();

        for (let i in this.position) {
            if (i !== "width" && i !== "height" && i !== "transform") {
                if (this.position[i] !== "") {
                    this.imageView.style[i] = this.position[i];
                } 
            }
        }
    }

    BuildCloseButton() {
        let btn = document.createElement('a');
        btn.className = "closebtn";
        btn.href = "#";
        return btn;
    }

    AddImageLoader(loader) {
        let scope = this;
        this.imageView.onload = function (e) {
            // console.log("MobileZoom:: AddImageLoader: e", e);
            scope.view.appendChild(scope.imageView);
            loader.parentNode.appendChild(scope.imageView);
            scope.initialImageData = new ObjectCords(scope.imageView);
            loader.parentNode.removeChild(scope.imageView);
            scope.view.appendChild(scope.closeBtn);
            // console.log("MobileZoom:: AddImageLoader: scope.initialImageData", scope.initialImageData);
            // console.log("MobileZoom:: AddImageLoader: scope.imageData.width", scope.imageData);
            loader.parentNode.appendChild(scope.imageView);
            loader.parentNode.removeChild(loader);
            scope.Scale();
            // scope.Center();
            scope.UpdatePosition();
            this.style.opacity = 1;

        }
    }

    Move(position) {
        let diffLeft = position.x - this.imageData.Left;
        let diffTop = position.y - this.imageData.Top;
        let offsetLeft = -1 * (((this.imageData.width - this.size.width) / 100) * (100 / this.image.Width * diffLeft));
        let offsetTop = -1 * (((this.imageData.height - this.size.height) / 100) * (100 / this.image.Height * diffTop));
        this.imageView.style.top = offsetTop + "px";
        this.imageView.style.left = offsetLeft + "px";
    }
}

module.exports = MobileZoom;