
import ZoomBase from "../shared/ZoomBase";
import ZoomViewPosition from "../shared/ZoomViewPosition";
import MouseView from "../shared/MouseView";
import ObjectCords from "../tools/ObjectCords";


class Zoom extends ZoomBase {

    constructor(image, options) {
        super(image, options);
        this.viewPosition = new ZoomViewPosition();
        this.mouseView = new MouseView();
    }
    
    Init() {
        this.initialImageData = new ObjectCords(this.imageView);
        this.imageData = new ObjectCords(this.imageView);

        if ( this.initialImageData.width == this.image.Width ) {
            this.initialImageData.width = this.initialImageData.width * 2;
            this.initialImageData.height = this.initialImageData.height * 2;
            this.imageView.style.width = this.initialImageData.width + 'px';
            this.imageView.style.height = this.initialImageData.height + 'px';
            this.initialImageData = new ObjectCords(this.imageView);
            this.imageData = new ObjectCords(this.imageView);
        }
        this.viewPosition.Init(this.view, this.image, this.options);
        // let scaleX = 1 / this.initialImageData.Width * this.viewPosition.Width;
        // let scaleY = 1 / this.initialImageData.Height * this.viewPosition.Height;
        this.mouseView.Init(this.image, this.options);
        // this.mouseView.Resize(scaleX, scaleY);
        this.viewPosition.Update();
        this.Resize();
        this.imageView.style.opacity = 1;
    }

    Resize() {
        this.imageData = new ObjectCords(this.imageView);
        let scaleX = 1 / this.initialImageData.Width * this.viewPosition.Width;
        let scaleY = 1 / this.initialImageData.Height * this.viewPosition.Height;
        this.mouseView.Resize(scaleX, scaleY);
        this.viewPosition.Resize();
    }

    Close() {
        this.viewPosition.Close();
        this.mouseView.Close();
    }

    Move(position) {
        let diffLeft = position.x - this.image.Left;
        let diffTop = position.y - this.image.Top;
        let offsetLeft = -1 * ( ( ( this.imageData.width - this.viewPosition.width ) / 100 ) * ( 100 / this.image.Width * diffLeft ));
        let offsetTop = -1 * ( ( ( this.imageData.height - this.viewPosition.height ) / 100 ) * ( 100 / this.image.Height * diffTop ));
        this.imageView.style.top = offsetTop + "px";
        this.imageView.style.left = offsetLeft + "px";
        this.mouseView.Move(position);
    }
}

module.exports = Zoom;