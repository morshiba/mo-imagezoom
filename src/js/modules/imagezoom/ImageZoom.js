

import Image from './tools/Image';
import Options from './tools/Options';
import AttributReader from './tools/AttributReader';
import MouseEvents from './events/MouseEvents';
import ResizeEvent from './events/ResizeEvent';
import Zoom from './zoom/Zoom';
import Mobile from './tools/Mobile';


var MouseEventHandler = null;

class ImageZoom {

    constructor(image) {
        this.image = null;
        this.zoom = null;
        let attributReader = new AttributReader();
        this.options = new Options();
        this.image = new Image(image);
        this.options.Image = this.image;
        this.options = attributReader.Parse(image, this.options);
        this.closeTimeout = null;
        this.mobile = new Mobile();

        this.Init(image);

        this.lastMobileState = this.mobile.IsMobile;
        this.mouseEventHandler = null;
        this.resizeEvent = new ResizeEvent(this.OnResize.bind(this));
    }

    Init() {
        MouseEventHandler = new MouseEvents(this.image.DOM, this.ToggleZoom.bind(this));
    }

    OnResize() {
        if (!this.mobile.IsMobile ) {
            this.image = new Image(this.image.DOM);
            this.options.ZoomViewPosition = "";
            if (this.zoom !== null) {
                this.zoom.Resize();
            }
        }
    }

    ToggleZoom(state, data) {
        if (state == "over") {
            if (!this.mobile.IsMobile ) {
                if (this.closeTimeout !== null) {
                    clearTimeout(this.closeTimeout);
                    this.closeTimeout = null;
                }
                if (this.zoom === null) {
                    this.zoom = new Zoom(this.image, this.options);
                }
                this.zoom.Move(data);
            }
        } else if (state == "out") {
            if (this.zoom !== null) {
                this.closeTimeout = setTimeout(function () {
                    this.closeTimeout = null;
                    this.zoom.Close();
                    this.zoom = null;
                }.bind(this), 1500);
            }
        } else if (state == "move") {
            if (this.zoom !== null) {
                this.zoom.Move(data);
            }
        }
    }
}

module.exports = ImageZoom;