



import ImageZoom from './modules/imagezoom/ImageZoom';

class MoImageZoom {

    constructor() {
        this.Init();
    }

    Init() {
        let images = document.querySelectorAll('.moimagezoom');
        for ( let i = 0; i < images.length; i++ ) {
            if ( !images[i].classList.contains('initialized') ) {
                new ImageZoom(images[i]);
                images[i].classList.add("initialized");
            }
        }
    }
}

module.exports = MoImageZoom;
global.MoImageZoom = new MoImageZoom();
