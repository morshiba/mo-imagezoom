





module.exports = function (grunt) {

	const pkg = grunt.file.readJSON('package.json');

	grunt.initConfig({
		pkg: pkg,
		browserify: {
			dist: {
				files: {
					// destination for transpiled js : source js
					'build/js/<%= pkg.name %>.js': 'src/js/index.js',
					// 'build/js/example.js': 'src/js/example.js'
				},
				options: {
					transform: ['babelify'],
					browserifyOptions: {
						debug: false
					}
				}
			}
		},
		uglify: {
			dist: {
				files: {
					'./build/js/<%= pkg.name %>.min.js': ['build/js/<%= pkg.name %>.js'],
					// './build/js/example.min.js': ['build/js/example.js']
				}
			}
		},

		sass: {
			frontend: {
				options: {
					style: 'expanded',
					"no-source-map": ''
				},
				files: [{
					expand: true,
					cwd: 'src/scss',
					src: ['**/*.scss'],
					dest: './build/css/',
					ext: '.css'
				}]
			}
		},

		autoprefixer: {
			bundle: {
				options: {
					browsers: ['last 2 versions', 'opera 12', 'ff 15', 'chrome 25', 'ie 11'] // ,
				},
				single_file: {
					src: './build/css/*.css',
					dest: './build/css/tmp.css'
				}
			},
			// countto: {
			// 	options: {
			// 		browsers: ['last 2 versions', 'opera 12', 'ff 15', 'chrome 25', 'ie 11'] // ,
			// 	},
			// 	single_file: {
			// 		src: './build/css/style.countto.min.css',
			// 		dest: './build/css/style_countto.css'
			// 	}
			// },
			// counter: {
			// 	options: {
			// 		browsers: ['last 2 versions', 'opera 12', 'ff 15', 'chrome 25', 'ie 11'] // ,
			// 	},
			// 	single_file: {
			// 		src: './build/css/style.counter.min.css',
			// 		dest: './build/css/style_counter.css'
			// 	}
			// }
		},

		cssmin: {
			options: {
				mergeIntoShorthands: false,
				roundingPrecision: -1
			},
			target: {
				files: [{
					expand: true,
					cwd: './build/css',
					src: ['style.css'],
					dest: './build/css',
					ext: '.min.css'
				}]
			}
		},

		watch: {
			css: {
				files: ['src/scss/**/*.scss'],
				tasks: ['clean:cssbefore', 'sass', 'autoprefixer:bundle', 'copy:renamecss', 'cssmin', 'clean:css']
			},
			js: {
				files: ['src/js/**/*.js'],
				// tasks: ['concat', 'uglify'],
				tasks: ['browserify:dist', 'uglify']
			},
			html: {
				files: ['src/html/**/*.html'],
				tasks: ['newer:copy:html']
			}
		},

		copy: {
			html: {
				files: [
					{ expand: true, cwd: 'src/html', src: ['**'], dest: './build' },
				]
			},
			renamecss: {
				files: [
					{
						expand: true,
						cwd: 'build/css/',
						src: ['tmp.css'],
						dest: './build/css/',
						rename: function (dest) {
							return dest + 'style.css';
						}
					},
				]
			}
		},
		clean: {
			css: ['./build/css/tmp.css'],
			cssbefore: ['./build/css/*.css']
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-newer');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-clean');
	// grunt.loadNpmTasks('babelify');
	grunt.loadNpmTasks('grunt-browserify');

	grunt.registerTask('default', ['watch']);
	// grunt.registerTask('watch', ['watch']);
	grunt.registerTask('build', ['clean:cssbefore', 'sass', 'autoprefixer:bundle', 'copy:renamecss', 'cssmin', 'clean:css', 'browserify:dist', 'uglify', 'copy:html']);

};